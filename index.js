// console.log("hello");

// [ houseNumber, street, barangay, municipality, Province, zipCode ]
// { name, species, weight, measurement }


/*--3-4--*/
let getCube = function(num){
	console.log(`The cube of ${num} is ${Math.pow(num,3)}.`);
}
getCube(2);


/*--5-6--*/
let address = ["KM22","East Service Road","South Superhighway","Cupang","Muntinlupa","1771"];
let [houseNumber,street,barangay,municipality,Province,zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${barangay} ${municipality} ${Province}, ${zipCode}`);


/*--7-8--*/
let animal = {
	name: "Plonks",
	species: "Dog",
	weight: 75, //pounds
	measurement: 24.5 //inches(height)
}

let {name,species,weight,measurement} = animal;

console.log(`${animal.name} is a ${animal.species} who weighs ${animal.weight} lbs. and is ${animal.measurement} inches tall.`);


/*--9-10--*/
let numbers = [0,2,5,6,7];

// console.log(numbers);
// numbers.forEach(function(num){
// 	console.log(num)
// })

numbers.forEach((num) => 
	console.log(num));


/*--11--*/
// console.log(numbers);

// let reducedNumber = numbers.reduce(function(x, y){
// 	return x + y;
// })
// console.log(reducedNumber);

let reducedNumber = numbers.reduce(( x , y ) => {
	return x + y;
})
console.log(reducedNumber);

/*--12-13--*/

class Dog{
	constructor(name,age,breed){
		this.goodBoy = name,
		this.dogAge = age,
		this.dogBreed = breed
	}
}

let Ping = new Dog("Raphael",2,"Labrador");
console.log(Ping);